const conexao = require('../databases/conexao')

class Pessoa {
    adicionar(pessoa, res) {
        const sql = 'INSERT INTO pessoa SET ?'

        conexao.query(sql, pessoa, (error, result) => {
            if (error) {
                res.status(400).json(error)
            } else {
                res.status(201).json(result)
            }
        })
    }

    listar(res) {
        const sql = 'SELECT * FROM pessoa'

        conexao.query(sql, (error, result) => {
            if(error) {
                res.status(400).json(error)
            } else {
                res.status(200).json(result)
            }
        })
    }

    buscarPorId(id, res) {
        const sql = `SELECT * FROM pessoa WHERE id_pessoa = ${id}`

        conexao.query(sql, (error, result) => {
            if(error) {
                res.status(400).json(error)
            } else {
                res.status(200).json(result[0])
            }
        })
    }

    editar(id, pessoa, res) {
        const sql = 'UPDATE pessoa SET ? WHERE id_pessoa = ?'

        conexao.query(sql, [pessoa, id], (error, result) => {
            if (error) {
                res.status(400).json(error)
            } else {
                res.status(200).json(result)
            }
        })
    }

    deletar(id, res) {
        const sql = 'DELETE FROM pessoa WHERE id_pessoa = ?'

        conexao.query(sql, id, (error, result) => {
            if (error) {
                res.status(400).json(error)
            } else {
                res.status(200).json(result)
            }
        })
    }

    buscarPorMatricula(id, res) {
        const sql = `SELECT * FROM pessoa WHERE nb_matricula = ${id}`

        conexao.query(sql, (error, result) => {
            if(error) {
                res.status(400).json(error)
            } else {
                res.status(200).json(result)
            }
        })
    }

    buscarPorNome(nome, res) {
        const sql = `SELECT * FROM pessoa WHERE tx_nome like '%${nome}%'`

        conexao.query(sql, (error, result) => {
            if(error) {
                res.status(400).json(error)
            } else {
                res.status(200).json(result)
            }
        })
    }

    buscarPorCPF(cpf, res) {
        const sql = `SELECT * FROM pessoa WHERE tx_cpf like '%${cpf}%'`

        conexao.query(sql, (error, result) => {
            if(error) {
                res.status(400).json(error)
            } else {
                res.status(200).json(result)
            }
        })
    }
}

module.exports = new Pessoa