const Pessoa = require('../models/pessoa')

module.exports = app => {
    app.get('/pessoas', (req, res) => {
        Pessoa.listar(res)
    })

    app.get('/pessoas/:id', (req, res) => {
        const id = parseInt(req.params.id)
        Pessoa.buscarPorId(id, res)
    })

    app.post('/pessoas', (req, res) => {
        const pessoa = req.body

        Pessoa.adicionar(pessoa, res)
    })

    app.patch('/pessoas/:id', (req, res) => {
        const id = parseInt(req.params.id)
        const pessoa = req.body

        Pessoa.editar(id, pessoa, res)
    })

    app.delete('/pessoas/:id', (req, res) => {
        const id = parseInt(req.params.id)

        Pessoa.deletar(id, res)
    })

    app.get('/pessoas/matricula/:id', (req, res) => {
        const id = parseInt(req.params.id)
        Pessoa.buscarPorMatricula(id, res)
    })

    app.get('/pessoas/nome/:nome', (req, res) => {
        const nome = req.params.nome
        Pessoa.buscarPorNome(nome, res)
    })

    app.get('/pessoas/cpf/:cpf', (req, res) => {
        const cpf = req.params.cpf
        Pessoa.buscarPorCPF(cpf, res)
    })
}