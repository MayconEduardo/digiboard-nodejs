class Tabelas {
    init(conexao) {
        this.conexao = conexao

        this.criarPessoa()
    }

    criarPessoa() {
        const sql = 'CREATE TABLE IF NOT EXISTS pessoa (id_pessoa int NOT NULL AUTO_INCREMENT, tx_nome varchar(50) NOT NULL, nb_matricula int NOT NULL, tx_cpf varchar(14), tx_foto text, PRIMARY KEY(id_pessoa))'


        this.conexao.query(sql, error => {
            if (error) {
                console.log(error)
            } else {
                console.log('Tabela criada com sucesso')
            }
        })
    }
}

module.exports = new Tabelas