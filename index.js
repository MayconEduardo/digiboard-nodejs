const customExpress = require('./config/customExpress')
const conexao = require('./databases/conexao')
const Tabelas = require('./databases/tabelas')

conexao.connect(error => {
    if (error) {
        console.log(error)
    } else {
        console.log('Banco de dados iniciado')

        Tabelas.init(conexao)
        const app = customExpress()

        app.listen(3000, () => console.log('Servidor iniciado.'))
    }
})

